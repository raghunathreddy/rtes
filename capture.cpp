


#include <pthread.h>
#include <stdio.h>
#include <sched.h>
#include <stdlib.h>
#include <semaphore.h>
#include <errno.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/contrib/contrib.hpp"


using namespace cv;
using namespace std;

int lowThreshold=0;
int const max_lowThreshold = 100;
int kernel_size = 3;
int edgeThresh = 1;
int ratio = 3;
Mat canny_frame, cdst, timg_gray, timg_grad;

#define NUM_THREADS	5
#define SEQ_SERVICE 	0 //SEQUENCER SERVICE
#define CANNY_SERVICE 	 1
#define HOUGH_SERVICE    2
#define ELIPTICAL_SERVICE 3
#define SOBEL_SERVICE 4
#define NSEC_PER_SEC (1000000000)
#define DELAY_TICKS (1)
#define ERROR (-1)
#define OK (0)
#define AVG_COUNT 200
#define HRES_STR "640"
#define VRES_STR "480"

char ppm_header[]="P6\n#9999999999 sec 9999999999 msec \n"HRES_STR" "VRES_STR"\n255 \n";
char ppm_dumpname[]="test00000000.ppm";



//Global Variable 
void end_delay_test(void);
unsigned long a = 0;
unsigned long n = 1;

unsigned long b = 0;
unsigned long m = 1;

double c = 0;
double o = 1;

unsigned int tt = 1;
unsigned int uu = 1;
int dumpfd =0;
int written=0;

//For Frame Rate
static struct timespec delta_time={0,0};
static struct timespec frame_start_time={0,0};
static struct timespec canny_time={0,0};
static struct timespec hough_time={0,0};
static struct timespec eliptical_time={0,0};
static struct timespec sobel_time={0,0};
static struct timespec frame_stop_time ={0,0};
struct timespec frame_time;
struct timespec frame_time_jpeg;
struct timespec frame_time_ten;
double curr_frame_time;
double curr_frame_time_jpeg;
double curr_frame_time_ten;
pthread_t threads[NUM_THREADS];
pthread_attr_t rt_sched_attr[NUM_THREADS];
int rt_max_prio, rt_min_prio;
struct sched_param rt_param[NUM_THREADS];
struct sched_param nrt_param;
struct sched_param main_param;
struct timespec timeNow;
int abortTest = 0;
static unsigned int sleep_count = 0;
char snapshotname[80]="snapshot_xxx.ppm";
char snapname[80]="tenhertz_xxx.ppm";


Mat houghTempframe, elipticalTempframe, cannyTempframe,sobelTempframe;


//Semaphores
//sem_t semF10,semF20;
pthread_mutex_t frame_mutex;


int rt_protocol;
volatile int runInterference=0, CScount=0;
volatile unsigned long long idleCount[NUM_THREADS];
int intfTime=0;

//CPU Set for affinity
cpu_set_t cpuset;	


void *startService(void *threadid);





// To find Delta value between start and stop time 
int delta_t(struct timespec *stop, struct timespec *start, struct timespec *delta_t)
{
  int dt_sec=stop->tv_sec - start->tv_sec;
  int dt_nsec=stop->tv_nsec - start->tv_nsec;

  if(dt_sec >= 0)
  {
    if(dt_nsec >= 0)
    {
      delta_t->tv_sec=dt_sec;
      delta_t->tv_nsec=dt_nsec;
    }
    else
    {
      delta_t->tv_sec=dt_sec-1;
      delta_t->tv_nsec=NSEC_PER_SEC+dt_nsec;
    }
  }
  else//why no change in assignment
  {
    if(dt_nsec >= 0)
    {
      delta_t->tv_sec=dt_sec;
      delta_t->tv_nsec=dt_nsec;
    }
    else
    {
      delta_t->tv_sec=dt_sec-1;
      delta_t->tv_nsec=NSEC_PER_SEC+dt_nsec;
    }
  }

  return(OK);
}

static struct timespec delay_error = {0, 0};


//Global variables to all threads 
IplImage* frame;
CvCapture* capture;

int motion_detection(void)
{

 Mat src, src_gray;

  /// Read the image
  
  namedWindow( "Hough Circle Transform Demo", CV_WINDOW_AUTOSIZE );
  int start_flag=0;
  
  while(start_flag==0)
 {
  if(cvGrabFrame(capture)) frame=cvRetrieveFrame(capture);
     
  if(!frame) break;
  cvSaveImage("circle.jpg", frame);
  src = imread( "circle.jpg", 1 );

  if( !src.data )
    { return -1; }
  

  /// Convert it to gray
  cvtColor( src, src_gray, CV_BGR2GRAY );

  /// Reduce the noise so we avoid false circle detection
  GaussianBlur( src_gray, src_gray, Size(9, 9), 2, 2 );

  vector<Vec3f> circles;
  

  /// Apply the Hough Transform to find the circles
  HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 200, 100, 0, 0 );
  

  /// Draw the circles detected
  for( size_t i = 0; i < circles.size(); i++ )
  {
      Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
      int radius = cvRound(circles[i][2]);
      // circle center
      circle( src, center, 3, Scalar(0,255,0), -1, 8, 0 );
      // circle outline
      circle( src, center, radius, Scalar(255,0,0), 3, 8, 0 );
      start_flag=1;
   }

  /// Show your results
  printf("I think i am here\n");
  imshow( "Hough Circle Transform Demo", src );
  waitKey(1);
  }


}


// Function for CANNY Threshold 
void CannyThreshold(int, void*)
{
    Mat mat_frame(frame);
    //mat_frame.copyTo(houghTempframe);   	
    cvtColor(mat_frame, timg_gray, CV_RGB2GRAY);
    /// Reduce noise with a kernel 3x3
    blur( timg_gray, canny_frame, Size(3,3) );
 	
    /// Canny detector
    Canny( canny_frame, canny_frame, lowThreshold, lowThreshold*ratio, kernel_size );
    /// Using Canny's output as a mask, we display our result
    cannyTempframe = Scalar::all(0);
    mat_frame.copyTo(cannyTempframe,canny_frame);
}	

// Function for SOBEL Thread
void * sobel(void *)
{
    
    //qprintf("Sobel Called\n\r");	
    //pthread_mutex_lock(&frame_mutex);
    clock_gettime(CLOCK_REALTIME, &frame_time_ten);
    /*			
    curr_frame_time_ten=(frame_time_ten.tv_sec * 1000.0)+(frame_time_ten.tv_nsec/1000000) ;
    printf("\nCurr_frame_time_ten is %lf",curr_frame_time_ten);
    printf("\nCurrent frame time is seconds=%ld,nano_seconds=%lf\n",frame_time_ten.tv_sec,((double)frame_time_ten.tv_nsec) );

    if(c==0)
   {
	c = curr_frame_time_ten;
	printf("\n Value of C is %lf", c);
   }
   else
   {
	o = curr_frame_time_ten;
	printf("\n Value of C is %lf", c);
	printf("\n Value of O is %lf", o);
	printf("\n Value of O-C is %lf", (o-c));

	if((o-c)>=100)
	{
           printf("Taking snapshot number %d\n",uu);
           sprintf(&snapname[9], "%8.4lf.ppm",uu );
	  
           cvSaveImage(snapname, frame);
           c = curr_frame_time_ten;
           o = 0;
	   uu++;
	}
    }

   
    pthread_mutex_unlock(&frame_mutex);	
     */	
    uu++;	
    clock_gettime(CLOCK_REALTIME, &sobel_time);   
    delta_t(&sobel_time, &frame_start_time, &delta_time);
    printf("\nSobel completed entry DT seconds = %ld, nanoseconds = %ld\n", delta_time.tv_sec, delta_time.tv_nsec);
  
}



// Function for CANNY Thread
void * canny(void *)
{
    //printf("Canny Called\n\r");	
    pthread_mutex_lock(&frame_mutex);			
    clock_gettime(CLOCK_REALTIME, &frame_time_jpeg);
   curr_frame_time_jpeg=((double)frame_time_jpeg.tv_sec * 1000.0) + ((double)((double)frame_time_jpeg.tv_nsec / 1000000.0));	
   printf("\nCurrent frame time is seconds=%ld,nano_seconds=%ld\n",frame_start_time.tv_sec,((double)frame_start_time.tv_nsec) );
 
   if(b==0)
   {
	b = (unsigned)frame_time_jpeg.tv_sec;
	printf("\n Value of A is %u", a);
   }
   else
   {
	m = (unsigned)frame_time_jpeg.tv_sec;
	printf("\n Value of B is %lu", b);
	printf("\n Value of M is %lu", m);
	printf("\n Value of B-M is %lu", (m-b));

	if((m-b)==1)
	{
           printf("Taking snapshot number\n");
           sprintf(&snapshotname[9], "%8.4lf.jpg",curr_frame_time );
	  
           cvSaveImage(snapshotname, frame);
           b = (unsigned)frame_time_jpeg.tv_sec;
           m = 0;
         
	}
   }
    pthread_mutex_unlock(&frame_mutex);			
    //printf("Completed Canny\n\r");		
    clock_gettime(CLOCK_REALTIME, &canny_time);
    delta_t(&canny_time, &frame_start_time, &delta_time);
    printf("Canny completed entry DT seconds = %ld, nanoseconds = %ld\n", delta_time.tv_sec, delta_time.tv_nsec);
    //Printing difference in time for execution of canny 
    
}



// Function for HOUGH Thread

void * hough(void *)
{ 
  
   pthread_mutex_lock(&frame_mutex);				
   //Mat mat_frame(frame);
   //frame.copyTo(houghTempframe);  
   clock_gettime(CLOCK_REALTIME, &frame_time);
   curr_frame_time=((double)frame_time.tv_sec * 1000.0) + ((double)((double)frame_time.tv_nsec / 1000000.0));	
   printf("\nCurrent frame time is seconds=%ld,nano_seconds=%ld\n",frame_start_time.tv_sec,((double)frame_start_time.tv_nsec) );
 
   if(a==0)
   {
	a = (unsigned)frame_time.tv_sec;
	printf("\n Value of A is %u", a);
   }
   else
   {
	n = (unsigned)frame_time.tv_sec;
	printf("\n Value of A is %lu", a);
	printf("\n Value of N is %lu", n);
	printf("\n Value of N-A is %lu", (n-a));

	if((n-a)==1)
	{
        printf("Taking snapshot number %d\n",tt);
	dumpfd = open(ppm_dumpname, O_WRONLY | O_NONBLOCK | O_CREAT, 00666);
	snprintf(&ppm_dumpname[4], 9, "%08d", tt);
	strncat(&ppm_dumpname[12], ".ppm", 5);
	snprintf(&ppm_header[4], 11, "%010d", (int)frame_time_jpeg.tv_sec);
	strncat(&ppm_header[14], " sec ", 5);
	snprintf(&ppm_header[19], 11, "%010d", (int)((frame_time_jpeg.tv_nsec)/1000000));
	strncat(&ppm_header[29], " msec \n"HRES_STR" "VRES_STR"\n255\n", 19);
	written=write(dumpfd, ppm_header, sizeof(ppm_header));
	close(dumpfd);
        cvSaveImage(ppm_dumpname, frame);
	a = (unsigned)frame_time.tv_sec;
        n = 0;
	tt++;

	}
   }
   pthread_mutex_unlock(&frame_mutex);			
   clock_gettime(CLOCK_REALTIME, &hough_time);
   delta_t(&hough_time, &frame_start_time, &delta_time);
   printf("Frame completed entry DT seconds = %ld, nanoseconds = %ld\n", delta_time.tv_sec, delta_time.tv_nsec);
   
  
}


    Mat gray;
    vector<Vec3f> circles;

//Function for Eliptical Thread 
void * eliptical (void *)
{

	//printf("\n\r Eliptical Thread ");
       pthread_mutex_lock(&frame_mutex);				
        Mat mat_frame(frame);
	//Make a copy to keep the original frame intact 
	mat_frame.copyTo(elipticalTempframe);  
	cvtColor(elipticalTempframe, gray, CV_BGR2GRAY);

	//Blurs to remove noise and fine it 
        GaussianBlur(gray, gray, Size(9,9), 2, 2);
        
	//Finds circles in a grayscale image using the Hough transform
        HoughCircles(gray, circles, CV_HOUGH_GRADIENT, 1, gray.rows/8, 100, 50, 0, 0);

	//Number of circles 
        printf("circles.size = %d\n", circles.size());

        //To draw raduis and its circles on original image  
        for( size_t i = 0; i < circles.size(); i++ )
        {
          Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
          int radius = cvRound(circles[i][2]);

          // circle outline
          circle( elipticalTempframe, center, radius, Scalar(0,0,255), 5, 8, 0 );
          // circle center
          circle( elipticalTempframe, center, 3, Scalar(0,255,0), -1, 8, 0 );
        }
       pthread_mutex_unlock(&frame_mutex);		
    clock_gettime(CLOCK_REALTIME, &eliptical_time);		
    delta_t(&eliptical_time, &frame_start_time, &delta_time);
    printf("Eliptical completed entry DT seconds = %ld, nanoseconds = %ld\n", delta_time.tv_sec, delta_time.tv_nsec);
  
}



//Function to Print the scheduler used 
int CScnt=0;
void print_scheduler(void)
{
   int schedType;
   schedType = sched_getscheduler(getpid());
   switch(schedType)
   {
     case SCHED_FIFO:
	   printf("Pthread Policy is SCHED_FIFO\n");
	   break;
     case SCHED_OTHER:
	   printf("Pthread Policy is SCHED_OTHER\n");
       break;
     case SCHED_RR:
	   printf("Pthread Policy is SCHED_OTHER\n");
	   break;
     default:
       printf("Pthread Policy is UNKNOWN\n");
   }
}

//main of program 
int main (int argc, char *argv[])
{
   int rc, invSafe=0, i, scope;
   struct timespec sleepTime, dTime;
   double FrameAvgTime=0,FrameSumTime=0;
   int iterations=0,Passed=0,Missed=0;
   int dev=0;// Default value for camera device 
   int resVal=0;//default value of resoultion
   double Jitter=0;             //

   //configuration values for resolution 
   int HRESArray[5]={80,160,320,640,1280};
   int VRESArray[5]={60,120,240,480,960};

   //configuration value for Deadline wrt to resolution 
   //Value set after calulating average for different resolution 
   int DeadlineArray[5]={65,12,25,62,65};

  int HRES;
  int VRES;
  double deadline;


  if (argc ==1)
  {
    printf("No Inputs using deafaults values\n");
  }
  else if (argc > 1)
  {
    if (argc >=2)
    {
      sscanf(argv[1] ,"%d",&dev);
      {
        if (argc ==3)
        {
          sscanf(argv[2] ,"%d",&resVal);   
        }
      }

    }
  }

  printf("%d %d %d \n",argv[1],argv[2]);
  if (resVal<5)
  {
   HRES=HRESArray[3];
   VRES=VRESArray[3];
   deadline =DeadlineArray[3];
  }
  else
   {
    HRES=HRESArray[3];
    VRES=VRESArray[3];
    deadline =DeadlineArray[3];
   } 

  printf("HRES %d VRES %d deadline %lf\n",HRES,VRES,deadline);




   printf("Main Started ");
   print_scheduler();

   //Setting attributtes of Services 
   //For Hough
   pthread_attr_setinheritsched(&rt_sched_attr[HOUGH_SERVICE], PTHREAD_EXPLICIT_SCHED);
   pthread_attr_init(&rt_sched_attr[HOUGH_SERVICE]);
   pthread_attr_setschedpolicy(&rt_sched_attr[HOUGH_SERVICE], SCHED_FIFO);


   //For Canny
   pthread_attr_init(&rt_sched_attr[CANNY_SERVICE]);
   pthread_attr_setinheritsched(&rt_sched_attr[CANNY_SERVICE], PTHREAD_EXPLICIT_SCHED);
   pthread_attr_setschedpolicy(&rt_sched_attr[CANNY_SERVICE], SCHED_FIFO);

   //For hough Eliptical
   pthread_attr_init(&rt_sched_attr[ELIPTICAL_SERVICE]);
   pthread_attr_setinheritsched(&rt_sched_attr[ELIPTICAL_SERVICE], PTHREAD_EXPLICIT_SCHED);
   pthread_attr_setschedpolicy(&rt_sched_attr[ELIPTICAL_SERVICE], SCHED_FIFO);	
	

   //For sobel
   pthread_attr_init(&rt_sched_attr[SOBEL_SERVICE]);
   pthread_attr_setinheritsched(&rt_sched_attr[SOBEL_SERVICE], PTHREAD_EXPLICIT_SCHED);
   pthread_attr_setschedpolicy(&rt_sched_attr[SOBEL_SERVICE], SCHED_FIFO);	

   //Find the max and min priority 
   rt_max_prio = sched_get_priority_max(SCHED_FIFO);
   rt_min_prio = sched_get_priority_min(SCHED_FIFO);
   rc=sched_getparam(getpid(), &nrt_param);
   main_param.sched_priority = rt_max_prio;

   //creating the main scheduler thread 
   rc=sched_setscheduler(getpid(), SCHED_FIFO, &main_param);
   print_scheduler();
   
   
   Mat gray, canny_frame, cdst;
  
   //Capture a Camera Device
   capture = (CvCapture*)cvCreateCameraCapture(dev);
   cvSetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH, HRES);
   cvSetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT, VRES);

  /*
  //Creating seperate windows for each transformation  
  cvNamedWindow( "Hough", CV_WINDOW_AUTOSIZE );
  cvNamedWindow( "Canny", CV_WINDOW_AUTOSIZE );
  //cvNamedWindow( "Eliptical", CV_WINDOW_AUTOSIZE );
  cvNamedWindow( "Sobel", CV_WINDOW_AUTOSIZE );
  cvNamedWindow( "Original", CV_WINDOW_AUTOSIZE );
*/

  //Initiate Mutex 
  pthread_mutex_init(&frame_mutex,NULL);

  //Set Priorities 
  rt_param[HOUGH_SERVICE].sched_priority = rt_max_prio-20;//Set priority of Low Priority Thread(79)
  pthread_attr_setschedparam(&rt_sched_attr[HOUGH_SERVICE], &rt_param[HOUGH_SERVICE]);//Set Schedule paramater of this thread

  rt_param[CANNY_SERVICE].sched_priority = rt_max_prio-30;//Set priority of Low Priority Thread(79)
  pthread_attr_setschedparam(&rt_sched_attr[CANNY_SERVICE], &rt_param[CANNY_SERVICE]);//Set Schedule paramater of this thread
  
  //rt_param[ELIPTICAL_SERVICE].sched_priority = rt_max_prio-30;//Set priority of Low Priority Thread(79)
  //pthread_attr_setschedparam(&rt_sched_attr[ELIPTICAL_SERVICE], &rt_param[ELIPTICAL_SERVICE]);//Set Schedule paramater of this thread

  rt_param[SOBEL_SERVICE].sched_priority = rt_max_prio-10;//Set priority of Low Priority Thread(79)
  pthread_attr_setschedparam(&rt_sched_attr[SOBEL_SERVICE], &rt_param[SOBEL_SERVICE]);//Set Schedule paramater of this thread

  motion_detection();

  while (1) 
  {
	//capture the frame
        clock_gettime(CLOCK_REALTIME, &frame_start_time);
	if(cvGrabFrame(capture)) frame=cvRetrieveFrame(capture);

	if(!frame) break;
  else
  {
    //Acquire start of frame 
                  
  }                                 




//Creating Threads 
    printf("Creating thread Hough Thread\n\r");		
rc = pthread_create(&threads[HOUGH_SERVICE], &rt_sched_attr[HOUGH_SERVICE], hough, (void *)HOUGH_SERVICE);
	if (rc)
	{
	     printf("ERROR: pthread_create() rc is hough service rc value %d" , rc);
	     perror(NULL);
	     exit(-1);
	}

        printf("Creating thread CANNY Thread\n\r");		
        rc = pthread_create(&threads[CANNY_SERVICE], &rt_sched_attr[CANNY_SERVICE], canny, (void *)CANNY_SERVICE);
	if (rc)
	{
	     printf("ERROR: pthread_create() rc is CANNY service rc value %d" , rc);
	     perror(NULL);
	     exit(-1);
	}
  
  /*
    printf("Creating thread ELIPTICAL Thread\n\r");		
    rc = pthread_create(&threads[ELIPTICAL_SERVICE], &rt_sched_attr[ELIPTICAL_SERVICE], eliptical, (void *)ELIPTICAL_SERVICE);
	if (rc)
	{
	     printf("ERROR: pthread_create() rc is ELIPTICAL service rc value %d" , rc);
	     perror(NULL);
	     exit(-1);
	}

*/      
      //printf("Creating thread SOBEL Thread\n\r");		
  rc = pthread_create(&threads[SOBEL_SERVICE], &rt_sched_attr[SOBEL_SERVICE], sobel, (void *)SOBEL_SERVICE);
	if (rc)
	{
	     printf("ERROR: pthread_create() rc is SOBEL service rc value %d" , rc);
	     perror(NULL);
	     exit(-1);
	}
 
	//Wating for thread executions to complete and joining them
	//hough
    
	if(pthread_join(threads[HOUGH_SERVICE], NULL) == 0)
	 {
	   //printf("HOUGH Thread completed\n");
	 }	
	 else
	 {
	   perror("HOUGH Thread Join");
	 }
       
         //Canny
	if(pthread_join(threads[CANNY_SERVICE], NULL) == 0)
	 {
	  //printf("CANNY Thread completed\n");
	 }	
	 else
	 {
	   perror("CANNY Thread Join");
	 }
   

	//ELIPTICAL
   /*
	if(pthread_join(threads[ELIPTICAL_SERVICE], NULL) == 0)
	 {
	  printf("ELIPTICAL Thread completed\n");
	 }	
	 else
	 {
	   perror("ELIPTICAL Thread Join");
	 }
   */
	//SOBEL
	/*
        if(pthread_join(threads[SOBEL_SERVICE], NULL) == 0)
	 {
	  //printf("SOBEL Thread completed\n");
	 }	
	 else
	 {
	   perror("SOBEL Thread Join");
	 }

	 //imshow( "Hough", houghTempframe );
	 //imshow( "Canny", cannyTempframe );
	 //imshow( "Eliptical", elipticalTempframe );
	 //imshow( "Sobel", sobelTempframe );
	 */
	 //cvShowImage("Original",frame);

	 //char q = cvWaitKey(100);
	 if( uu >=  200 )
	 {
      	printf("Quit resources and application \n"); 
	   //Exiting capture 	

	   break;

	   //exit(-1);	
	 }	 
    //printf("\n Recreate canny and hough\n\r");
    
    clock_gettime(CLOCK_REALTIME,&frame_stop_time);
    
    
    Jitter = (delta_time.tv_nsec/1000000) - deadline;
    if (Jitter >0 )
    {
      Missed++;
      printf("Deadline Missed\n");
    }
    else
    {
      Passed++;
      printf("Deadline Not Missed\n");
    }
    printf("*****Frame No: %d Jitter :%lf\n",iterations,Jitter);
    //To find average execution time 
    
      FrameSumTime +=delta_time.tv_nsec;
      iterations++;

    
    printf("\nstart frame time is seconds=%ld,nano_seconds=%lf\n",frame_start_time.tv_sec,((double)frame_start_time.tv_nsec) );
    printf("\nstop frame time is seconds=%ld,nano_seconds=%lf\n",frame_stop_time.tv_sec,((double)frame_stop_time.tv_nsec) );
    delta_t(&frame_stop_time, &frame_start_time, &delta_time);
    printf("completed all transformation seconds = %ld, nanoseconds = %ld\n", delta_time.tv_sec, delta_time.tv_nsec);

    
      
    
 } 

   //Calculate Average Time 
   FrameAvgTime=FrameSumTime/iterations;
      
   //Print Summary	  
   printf("************************************Summary************************\n\r");
   printf("Resolution %d*%d \n\r",HRES,VRES);
   printf("Average time for all Transformation %f ms\n\r",FrameAvgTime/1000000);
   printf("Frame Rate for all Transformation %f Hz\n\r",1000/(FrameAvgTime/1000000));
   printf("Total Frames : %d\n",iterations);
   printf("Deadline Missed : %d\nDeadline Not Missed: %d\n\r",Missed,Passed);
   printf("Number of time looped into the ten hertz frames is %d\n",uu);
   printf("*********************************END Summary************************\n\r");
   cvReleaseCapture(&capture);  	
   //Exiting Thread 
   
   return 0;
}



